﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	public CharacterData PlayerData;
	public float damage;
	public float defense;
	public float speed;
	public HealthBar healthBar;
	private float maxHealth;
	private float currentHealth;
	private UnityAction PlayerTakeDamage;



	// Start is called before the first frame update
	void Start()
    {
		damage = PlayerData.stats.damage;
		defense = PlayerData.stats.defense;
		speed = PlayerData.stats.moveSpeed;
		maxHealth = PlayerData.stats.maxHp;
		currentHealth = PlayerData.stats.maxHp;
		healthBar.SetMaxHealth(maxHealth);
    }
    private void OnEnable()
    {
		EventManager.OnHurtPlayer += TakeDamage;
		EventManager.OnHealPlayer += HealPlayer;
		EventManager.OnGetBuff += BuffPlayer;
	}
    private void OnDisable()
    {
		EventManager.OnHurtPlayer -= TakeDamage;
		EventManager.OnHealPlayer -= HealPlayer;
		EventManager.OnGetBuff -= BuffPlayer;
	}
    // Update is called once per frame
    void Update()
    {
		
    }

	public void TakeDamage(float damage)
	{
		damage /= defense;
		currentHealth -= damage;
		if(currentHealth <= 0)
        {
			SceneManager.LoadScene(4);
		}
		//Debug.Log("player");
		healthBar.SetHealth(currentHealth);
	}
	public void BuffPlayer(BuffItem item)
    {
		switch (item.buffType)
        {
			case Buff.ATTACK:
				
				damage += item.buffAmount;
				break;
			case Buff.DEFENSE:
				defense += item.buffAmount;
				break;
			case Buff.SPEED:
				speed += item.buffAmount;
				break;
			case Buff.MAXHEALTH:
				Debug.Log("buff max health" + item.buffAmount);
				break;
		}
    }
	public void HealPlayer(Potion potion)
	{
		float temp = currentHealth + potion.healAmount;
        if (temp> maxHealth)
        {
			currentHealth = maxHealth;

        }
        else
        {
			currentHealth += potion.healAmount;
		}
		//Debug.Log(currentHealth);

		healthBar.SetHealth(currentHealth);
	}
}
