using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipHandler : MonoBehaviour
{
    public static AudioClipHandler i;
    public AudioClip shootingSound;
    public SoundAudioClip[] soundAudioClipArray;
    [System.Serializable]
    public class SoundAudioClip
    {
        public SoundManager.Sound sound;
        public AudioClip audioClip;
    }
    public void PlayShootingSound()
    {
        GameObject soundGameObject = new GameObject("Sound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.PlayOneShot(shootingSound);
    }

}
