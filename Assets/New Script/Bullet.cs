using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody2D rb;
    public float bulletDamage;
    public GameObject explosionEffect;
    public void SetDamage(float damage)
    {
        bulletDamage = damage;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Enemy")
        {
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if(collision.transform.tag == "EnemyBullet")
        {
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
           Destroy(gameObject);
        }
    }
}
