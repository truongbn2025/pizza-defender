using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWeapon : MonoBehaviour
{
    public GameObject weaponOne;
    public GameObject weaponTwo;
    public GameObject weaponThree;

    private void Start()
    {
        weaponOne.SetActive(true);
        weaponTwo.SetActive(false);
        weaponThree.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weaponOne.SetActive(true);
            weaponTwo.SetActive(false);
           weaponThree.SetActive(false);
        }else
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weaponOne.SetActive(false);
            weaponTwo.SetActive(true);
            weaponThree.SetActive(false);
        }else
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            weaponOne.SetActive(false);
            weaponTwo.SetActive(false);
            weaponThree.SetActive(true);
        }

    }
}
