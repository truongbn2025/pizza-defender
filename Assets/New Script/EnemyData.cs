using UnityEngine;
[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObject/Enemy")]
public class EnemyData : ScriptableObject
{
    public int id;
    public Stats stats;
}
