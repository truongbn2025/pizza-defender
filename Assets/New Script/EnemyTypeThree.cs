using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTypeThree : Enemy
{
    public GameObject targetMove;
    public GameObject targetAttack;
    public float attackCooldown;
    private float baseAttackCooldown;
    private bool canAttack = false;
    private float currentHealth;
    private Rigidbody2D rb;
    public GameObject enemyProjectile;
    public List<GameObject> buffItems;
    private void Start()
    {
        targetMove = GameObject.FindGameObjectWithTag("MainBase");
        targetAttack = GameObject.FindGameObjectWithTag("MainBase");
        hp = enemyData.stats.maxHp;
        damage = enemyData.stats.damage;
        def = enemyData.stats.defense;
        speed = enemyData.stats.moveSpeed;
        type = enemyData.id;
        baseAttackCooldown = attackCooldown;
        currentHealth = hp;
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        EnemyMove();
        CountDown();
        //EnemyAttack();
    }
    private void LateUpdate()
    {
        if (hp <= 0)
        {

            gameObject.SetActive(false);
        }
    }
    private void CountDown()
    {
        attackCooldown -= Time.deltaTime;
        if ((attackCooldown) <= 0)
        {
            canAttack = true;
            attackCooldown = baseAttackCooldown;
        }
    }
    private void EnemyMove()
    {
        float step = this.speed * Time.deltaTime;
        Vector2 movement = targetMove.transform.position - gameObject.transform.position;
        movement.Normalize();
        rb.MovePosition(rb.position + movement * this.speed * Time.deltaTime);
        //gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetMove.transform.position, step);
    }

    private void EnemyAttack()
    {
        if (canAttack)
        {

            GameObject projectile = Instantiate(enemyProjectile, transform.position, transform.rotation * Quaternion.identity);
            projectile.GetComponent<EnemyBullet>().SetDamage(damage);
            projectile.GetComponent<EnemyBullet>().SetTarget(targetAttack.transform.position);
            canAttack = false;
        }
    }

    private void OnDisable()
    {
        //EventManager.OnEnemyKilled(scoreValue);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            TakeDamage(collision.gameObject.GetComponent<Bullet>().bulletDamage);
        }
        if (collision.gameObject.tag == "Staff")
        {
            TakeDamage(collision.gameObject.GetComponent<ExplosionStaff>().bulletDamage);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {

            if (canAttack)
            {

                EventManager.OnHurtPlayer(this.damage);
            }
        }
        if (collision.gameObject.tag == "MainBase")
        {
            if (canAttack)
            {
                //EventManager.OnHurtPlayer(this.damage);
            }
        }
        if (collision.gameObject.tag == "PlayerBullet")
        {
            TakeDamage(collision.gameObject.GetComponent<Bullet>().bulletDamage);
        }
    }

    public void TakeDamage(float damageTaken)
    {
        //Debug.Log(damageTaken);
        hp -= damageTaken;
        if (hp <= 0)
        {

            int type = Random.Range(0, buffItems.Count);
            GameObject newEnemy = Instantiate(buffItems[type], transform.position, Quaternion.identity);
            //Debug.Log("killed");
        }
    }
}
