using System;

public static class EventManager
{
    public static Action<int> OnEnemyKilled;
    public static Action<float> OnHurtPlayer;
    public static Action<Potion> OnHealPlayer;
    public static Action<BuffItem> OnGetBuff;
    public static Action<int> OnNextWave;
    public static Action isFiring;
}