using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionStaff : MonoBehaviour
{
    public Rigidbody2D rb;
    public float bulletDamage;
    public GameObject explosionEffect;
    public void SetDamage(float damage)
    {
        bulletDamage = damage;
    }

}
