using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerMovement : MonoBehaviour
{
    public Camera cam;
    public CharacterData PlayerData;
    public Rigidbody2D rb;
    Weapon weapon;
    Vector2 movement;
    Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
        Debug.Log(PlayerData.stats.damage);
        //weapon = GetComponent<Weapon>();
    }
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        if (Input.GetMouseButtonDown(0))
        {
            EventManager.isFiring();
            //weapon.Fire();
        }
        if(movement.magnitude != 0)
        {
            anim.SetBool("isMove", true);
        }
        else
        {
            anim.SetBool("isMove", false);
        }
       
        

    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        rb.MovePosition(rb.position + movement * gameObject.GetComponent<Player>().speed * Time.fixedDeltaTime);
        Vector2 mousePos = Input.mousePosition;
        Vector3 mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        if (mouseWorldPosition.x > this.gameObject.transform.position.x)
        {
            //Debug.Log(mousePos.x + " " + this.gameObject.transform.position.x);
            anim.SetBool("ChangeLook", true);
        }
        else
        {
            anim.SetBool("ChangeLook", false);
        }
    }


}

