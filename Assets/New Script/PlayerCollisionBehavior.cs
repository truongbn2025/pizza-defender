using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionBehavior : MonoBehaviour
{

    public AudioClip bonus;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.transform.tag == "HealPotion")
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(bonus);
            EventManager.OnHealPlayer(collision.gameObject.GetComponent<Potion>());
            Destroy(collision.gameObject);
        }
        if(collision.transform.tag == "Buff")
        {
            gameObject.GetComponent<AudioSource>().PlayOneShot(bonus);
            EventManager.OnGetBuff(collision.gameObject.GetComponent<BuffItem>());
            Destroy(collision.gameObject);
        }
    }
}
