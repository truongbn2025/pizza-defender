using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private int score;
    private void OnEnable()
    {
        EventManager.OnEnemyKilled += EnemyKilled;
    }

    private void OnDisable()
    {
        EventManager.OnEnemyKilled -= EnemyKilled;
    }

    public void EnemyKilled(int scoreValue)
    {
        score += scoreValue;
        Debug.Log(score);
    }
}
