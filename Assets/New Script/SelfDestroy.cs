using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour
{
    public float timer = 2f;

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, timer);
    }
}
