using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundManager
{
    public enum Sound
    {
        SHOOT,
        COIN,
        WALK,
        LOSE,
        WIN
    }
    public static void PlayShootingSound()
    {
        GameObject soundGameObject = new GameObject("Sound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.PlayOneShot(AudioClipHandler.i.shootingSound);
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        
        foreach(AudioClipHandler.SoundAudioClip soundAudioClip in AudioClipHandler.i.soundAudioClipArray)
        {
            if(soundAudioClip.sound == sound)
            {
                Debug.Log(soundAudioClip.sound + "true");
                return soundAudioClip.audioClip;
            }
            
        }
        return null;
    }
    
}
