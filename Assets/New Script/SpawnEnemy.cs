﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    //public SpawnEnemy Spawner;
    [SerializeField]private float baseSpawnCountdown;
    private float spawnCountDown;
    public GameObject enemyTypeOne;
    public GameObject enemyTypeTwo;
    public GameObject enemyTypeThree;
    private bool canSpawn;
    private int enemyType;
    public bool stopSpawning;

    // Start is called before the first frame update
    void Start()
    {
        canSpawn = true;
        //Spawner = this;
        //timeStep = 1;
        spawnCountDown = 5;
    }



    void SpawnCountDown()
    {
        //Debug.Log(spawnCountDown);
        spawnCountDown -= Time.deltaTime;
        if (spawnCountDown <= 0)
        {
            
            canSpawn = true;
            spawnCountDown = baseSpawnCountdown;
        }
    }

    void Spawn()
    {
        //Debug.Log("here");
        enemyType = Random.Range(1, 4);

        if (enemyType == 1)
        {
            
            GameObject newEnemy = Instantiate(enemyTypeOne, transform.position, Quaternion.identity);
         

        }
        if (enemyType == 2)
        {
           
            GameObject newEnemy = Instantiate(enemyTypeTwo, transform.position, Quaternion.identity);
          

        }
        if (enemyType == 3)
        {
            
            GameObject newEnemy = Instantiate(enemyTypeThree, transform.position, Quaternion.identity);
           
        }
    }

    // Update is called once per frame
    void Update()
    {

        SpawnCountDown();
        if (canSpawn)
        {
            Spawn();
            canSpawn = false;
        }

    }
  
}
