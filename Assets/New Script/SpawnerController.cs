using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public int wave;
    public List<GameObject> waveOneSpawner;
    public List<GameObject> waveTwoSpawner;
    public List<GameObject> waveThreeSpawner;
    private void OnEnable()
    {
        EventManager.OnNextWave += EnableSpawner;
    }
    private void OnDisable()
    {
        EventManager.OnNextWave -= EnableSpawner;
    }
    public void EnableSpawner(int waveNumber)
    {
        if (waveNumber > 0)
        {
            foreach (GameObject spawner in waveOneSpawner)
            {
                spawner.SetActive(true);
            }
        }
        if (waveNumber > 1)
        {
            foreach (GameObject spawner in waveTwoSpawner)
            {
                spawner.SetActive(true);
            }
        }
        if (waveNumber > 2)
        {
            foreach (GameObject spawner in waveThreeSpawner)
            {
                spawner.SetActive(true);
            }
        }

    }

}
