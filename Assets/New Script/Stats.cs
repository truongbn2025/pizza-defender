[System.Serializable]
public class Stats
{
    public float damage;
    public float defense;
    public float maxHp;
    public float moveSpeed;
}