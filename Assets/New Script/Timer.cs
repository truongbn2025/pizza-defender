using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class Timer : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn = false;
    public TMP_Text textDisplay;
    public Text TimerTxt;
    int waveNumber=0;
    float changeStageTime = 5;

    void Start()
    {
        TimerOn = true;
    }

    void Update()
    {
        if (TimerOn)
        {
            if (TimeLeft > 0)
            {
                if (changeStageTime > 0)
                {
                    changeStageTime -= Time.deltaTime;
                }
                else
                {
                    //Debug.Log("next stage");
                    waveNumber++;
                    EventManager.OnNextWave(waveNumber);
                    changeStageTime = 12;
                }
                TimeLeft -= Time.deltaTime;
                updateTimer(TimeLeft);
            }
            else
            {
                SceneManager.LoadScene(3);
                Debug.Log("Time is UP!");
                TimeLeft = 0;
                TimerOn = false;
            }
        }
    }

    void updateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        textDisplay.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

}