using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject player;
    public GameObject bullet;
    public Transform firePoint;
    public float fireForce;
    public AudioClip shootingSound;

    private void OnEnable()
    {
        EventManager.isFiring += Fire;
    }
    private void OnDisable()
    {
        EventManager.isFiring -= Fire;
    }

    public void Fire()
    {
        //AudioClipHandler.i.PlayShootingSound();
        gameObject.GetComponent<AudioSource>().PlayOneShot(shootingSound);
        GameObject projectile = Instantiate(bullet, firePoint.position, firePoint.rotation * Quaternion.identity);
        projectile.GetComponent<Bullet>().SetDamage(player.GetComponent<Player>().damage);
        projectile.GetComponent<Rigidbody2D>().AddForce(firePoint.up * fireForce, ForceMode2D.Impulse);
    }
}
