using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public Rigidbody2D rb;
    public float bulletDamage;
    private Vector2 targetPosition;
    public GameObject explosionEffect;

    public void SetDamage(float damage)
    {
        bulletDamage = damage;
    }
    public void SetTarget(Vector2 target)
    {
        targetPosition = target;
        
    }

    private void Update()
    {
        if(Vector2.Distance(targetPosition, transform.position) <= 0.01f)
        {
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, 0.5f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.transform.tag == "PlayerBullet")
        {
            //Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.transform.tag == "Player")
        {
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
            
            EventManager.OnHurtPlayer(bulletDamage);
            Destroy(gameObject);
        }
    }
}
