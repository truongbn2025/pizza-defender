using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour
{
    public GameObject player;
    public GameObject bullet;
    public Camera cam;
    //public Transform firePoint;
    //public float fireForce;
    //public AudioClip shootingSound;

    private void OnEnable()
    {
        EventManager.isFiring += Fire;
    }
    private void OnDisable()
    {
        EventManager.isFiring -= Fire;
    }

    public void Fire()
    {
        //AudioClipHandler.i.PlayShootingSound();
        //gameObject.GetComponent<AudioSource>().PlayOneShot(shootingSound);
        Vector2 mousePosition = Input.mousePosition;
        Vector2 objPosition = cam.ScreenToWorldPoint(mousePosition);
        GameObject projectile = Instantiate(bullet, objPosition, Quaternion.identity);
        projectile.GetComponent<ExplosionStaff>().SetDamage(player.GetComponent<Player>().damage);
        //projectile.GetComponent<Rigidbody2D>().AddForce(firePoint.up * fireForce, ForceMode2D.Impulse);
    }
}
